from django.urls import path

from . import views

urlpatterns = [
    path('', views.contact, name='contact'),
    path('detail/<int:cid>', views.detail, name='detail'),
    path('edit/<int:pers_id>', views.edit, name='edite'),
    path('listing/', views.contact_listing, name='listing'),
    path('delete/<int:pers_id>', views.delete, name='delete'),



]
